<footer class="page-footer font-small warning-color-dark text-white bg-warning">

  <div style="background-color: #CC0000;">
    <div class="container">
      <div class="row py-4 d-flex align-items-center">
        <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
          <h6 class="mb-0">Conheça nossa rede social</h6>
        </div>

        <div class="col-md-6 col-lg-7 text-center text-md-right">
          <a href="https://pt-br.facebook.com/PratoCheioRest/" class="fb-ic">
            <i class="fab fa-facebook-f white-text mr-4"> </i>
          </a>

        </div>
      </div>

    </div>
  </div>
  <div class="container text-center text-md-left mt-5">
    <div class="row mt-3">
      <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
        <h6 class="text-uppercase font-weight-bold">PRATO CHEIO</h6>
        <hr class="danger-color-dark accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        <p class="text-justify ">Nosso restaurante é de família, trabalhamos com comida caseira de ótima qualidade, venha nos conhecer e provar nossas deliciosas refeições!</p>
      </div>

      <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">

        <h6 class="text-uppercase font-weight-bold">Especialidades</h6>
        <hr class="danger-color-dark accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        <p>
          <a href="<?= base_url('index.php/categoria/') ?>">Refeições</a>
        </p>
        <p>
          <a href="<?= base_url('index.php/categoria/') ?>">Porções</a>
        </p>
        <p>
          <a href="<?= base_url('index.php/categoria/') ?>">Salgados</a>
        </p>
        <p>
          <a href="<?= base_url('index.php/categoria/') ?>">Feijoada</a>
        </p>

      </div>

      <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">

        <h6 class="text-uppercase font-weight-bold">LINKS</h6>
        <hr class="danger-color-dark accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        <p>
          <a href="<?= base_url("index.php") ?>">Home</a>
        </p>
        <p>
          <a href="<?= base_url("index.php/categoria/") ?>">Cardápio</a>
        </p>
        <p>
          <a href="<?= base_url("index.php/home/galeria") ?>">Galeria</a>
        </p>
        <p>
          <a href="<?= base_url("index.php/contato") ?>">Contato</a>
        </p>

      </div>

      <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

        <h6 class="text-uppercase font-weight-bold">Contato</h6>
        <hr class="danger-color-dark accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        <p>
          <i class="fas fa-home mr-3"></i>Av. Salgado Filho, 3574</p>
        <p>
          <i class="fas fa-city mr-3"></i>Guarulhos,SP</p>
        <p>
          <i class="fas fa-phone mr-3"></i><a href="tel:1126002502"> (11) 2600-2502</a></p>
      </div>

    </div>

  </div>
  <div class="footer-copyright text-center py-3">© 2019 Desenvolvido por:
    <a href="http://hospedagem.ifspguarulhos.edu.br/~gu1800175/myrandes/"> Myrandes</a>
  </div>
</footer>
<a class="scroll-to-top rounded" href="#">
  <i class="fas fa-angle-up"></i>
</a>