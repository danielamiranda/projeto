<div class="container">
<h1 class="mt-30 mt-0 mb-15 font-beyond text-center" style="font-size:3rem;">Galeria</h1> <br/>
  <div class="galeria">
    <div class="row">
      <div class="col-sm-12 col-md-4">
        <a class="lightbox" href="<?= base_url('assets/img/img01.jpeg') ?>">
          <img src="<?= base_url('assets/img/img01.jpeg') ?>">
        </a>
      </div>
      <div class="col-sm-12 col-md-4">
        <a class="lightbox" href="<?= base_url('assets/img/img02.jpeg') ?>">
          <img src="<?= base_url('assets/img/img02.jpeg') ?>">
        </a>
      </div>
      <div class="col-sm-12 col-md-4">
        <a class="lightbox" href="<?= base_url('assets/img/img03.jpeg') ?>">
          <img src="<?= base_url('assets/img/img03.jpeg') ?>">
        </a>
      </div>
      <div class="col-sm-12 col-md-4">
        <a class="lightbox" href="<?= base_url('assets/img/img05.jpeg') ?>">
          <img src="<?= base_url('assets/img/img05.jpeg') ?>">
        </a>
      </div>
      <div class="col-sm-12 col-md-4">
        <a class="lightbox" href="<?= base_url('assets/img/img06.jpeg') ?>">
          <img src="<?= base_url('assets/img/img06.jpeg') ?>">
        </a>
      </div>
      <div class="col-sm-12 col-md-4">
        <a class="lightbox" href="<?= base_url('assets/img/img07.jpeg') ?>">
          <img src="<?= base_url('assets/img/img07.jpeg') ?>">
        </a>
      </div>
      <div class="col-sm-12 col-md-4">
        <a class="lightbox" href="<?= base_url('assets/img/img08.jpeg') ?>">
          <img src="<?= base_url('assets/img/img08.jpeg') ?>">
        </a>
      </div>
      <div class="col-sm-12 col-md-4">
        <a class="lightbox" href="<?= base_url('assets/img/img10.jpeg') ?>">
          <img src="<?= base_url('assets/img/img10.jpeg') ?>">
        </a>
      </div>
      <div class="col-sm-12 col-md-4">
        <a class="lightbox" href="<?= base_url('assets/img/img11.jpeg') ?>">
          <img src="<?= base_url('assets/img/img11.jpeg') ?>">
        </a>
      </div>
    </div>
  </div>
</div>