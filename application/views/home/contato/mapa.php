<br><br>
<div class="container mt-5 pt-5">
    <div class="row">
        <div class="col-md-7 p-2 mapa">
            <div id="map-container-google-1" class="map-container">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14642.39933407327!2d-46.5367066!3d-23.4388184!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3ea456e45f51d8d!2sPrato%20Cheio%20-%20Restaurante%20e%20Bar!5e0!3m2!1spt-BR!2sbr!4v1571774443145!5m2!1spt-BR!2sbr" 
                    width="100%"  height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>
        </div>

        <div class="col-md-5 mx-auto mt-2">
            <p class="h2 texto-vermelho">RESTAURANTE PRATO CHEIO<p>
            <hr class="red darken-4">
            <p class="text-justify h5-responsive">Venha conhecer nosso restaurante e provar nossas deliciosas refeições!</p>
            <p class="text-justify h5-responsive"><i class="fas texto-vermelho fa-home mr-2"></i>Av. Salgado Filho, 3574 </p>
            <p class="text-justify h5-responsive"><i class="fas texto-vermelho fa-city mr-2"></i>Guarulhos, SP</p>
            <p class="text-justify h5-responsive"><i class="fas texto-vermelho fa-phone mr-2"></i>(11) 2600-2502</p>
        </div>
      
    </div>
</div>