<div style="margin-bottom:-50px">
    <div class="my-5 grey lighten-3 ifood">
        <div class="container mx-auto mb-0">
            <div class="row">
                <div class=" col-md-6 ">
                    <img src="<?= base_url('assets/mdb/img/phone.png') ?>" width="600" alt="ifood">
                </div>

                <div class="col-md-6  my-5 py-4 mx-auto">
                    <p class="h2 texto-vermelho">FAÇA SEU PEDIDO PELO IFOOD<p>
                            <hr class=" red darken-4">
                            <p class="text-justify h5-responsive">O Prato Cheio está no Ifood, o maior aplicativo especializado
                                em delivery na América Latina. </p>
                            <a href="https://www.ifood.com.br/delivery/guarulhos-sp/prato-cheio---restaurante-e-bar-centro/02051636-0af5-4025-9499-66a03d23f36f?UTM_Medium=share">
                                <button type="button" class="btn red darken-4 col-md-4 ">
                                    <b class="h6 text-white">PEÇA NO IFOOD</b></button>
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=br.com.brainweb.ifood&hl=pt_BR">
                                <button type="button" class="btn  grey darken-2 col-md-4">
                                    <b class="h6 text-white">BAIXE O APP</b></button>
                            </a>
                </div>
            </div>
        </div>
    </div>
</div>