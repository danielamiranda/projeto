<div class="modal fade" id="ModalHora" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title ml-3" id="exampleModalLabel">HORÁRIO DE FUNCIONAMENTO</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-justify ml-2">
                <p><b class="texto-vermelho">Segunda: </b> 11:00 - 15:00</p>
                <p><b class="texto-vermelho">Terça: </b> 11:00 - 15:00</p>
                <p><b class="texto-vermelho">Quarta: </b> 11:00 - 15:00</p>
                <p><b class="texto-vermelho">Quinta: </b> 11:00 - 15:00</p>
                <p><b class="texto-vermelho">Sexta: </b> 11:00 - 15:00</p>
                <p><b class="texto-vermelho">Sábado: </b> 11:00 - 16:00</p>
                <p><b class="texto-vermelho">Domingo: </b> 11:00 - 16:00</p>
                <hr class="danger-color-dark">
                <p><b class="texto-vermelho">Telefones para contato: </b></p>
                <i class="fas fa-phone-volume"></i><a href="tel:1126002502"> (11) 2600-2502</a><br />
                <i class="fab fa-whatsapp green-text"></i><a href="https://api.whatsapp.com/send?1=pt_BR&phone=5511972150750"> (11) 97215-0750</a>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>