<div class="container my-5">
    <p class="text-center font-beyond">
        <img src="<?=base_url('assets/mdb/img/historia.png')?>" width="50" alt="historia"><br><br>
    <b style="font-size: 3rem;"> Nossa História</b>
    </p>
    <div class="row text-justify">
        <div class="col-md-6">
        O Prato Cheio é um restaurante de família, começaram com uma lanchonete e trabalham a cerca de 6 anos. 
        Certo dia no trabalho, estavam fazendo seu próprio almoço, quando clientes presentes nos estabelecimentos
        ao redor sentiram o cheiro da comida e entraram na lanchonete para perguntar se trabalhavam com refeições,
        nesse momento vendiam apenas salgados e doces, mas após esse evento decidiram começar a trabalhar com refeições. 
        </div>
        
        <div class="col-md-6">
        Percebendo que a venda de comida era superior à de doces e salgados,
        resolveram se mudar para um espaço maior e abrir um restaurante. Gustavo, o filho, trabalhava em
        outro local no momento mas resolveu mudar seus planos para auxiliar os pais, seguindo com o ramo alimentício,
        mas dessa vez focando em pratos feitos e marmitas. Assim nasce o restaurante Prato Cheio.
        </div>
    </div>
</div>