-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 11-Dez-2019 às 21:39
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restaurante3`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `administrador`
--

CREATE TABLE `administrador` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `senha` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `administrador`
--

INSERT INTO `administrador` (`id`, `nome`, `email`, `senha`) VALUES
(1, 'Administrador', 'pratocheio@site.com', 'pr@t0031219');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cardapio`
--

CREATE TABLE `cardapio` (
  `id` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `categoria` int(11) NOT NULL,
  `descricao` text NOT NULL,
  `preco` char(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cardapio`
--

INSERT INTO `cardapio` (`id`, `foto`, `nome`, `categoria`, `descricao`, `preco`) VALUES
(1, '6d906be4d5ee6dfcf8cad4e7e773a0f7.jpg', 'Filé de frango', 2, 'Delicioso filé de frango, acompanha arroz, feijão e salada', '18.00'),
(2, '0bbcbbaa3307fa1441e65578e0aa7a72.jpg', 'Calabresa ', 2, 'Calabresa acebolada, acompanha arroz, feijão, salada e fritas', '18.00'),
(3, 'f29bae8a912e710342347008a28bda45.jpg', 'Picadinho', 2, 'Picadinho de carne com legumes, acompanha arroz, feijão, fritas e salada.', '20.00'),
(5, 'bc292359b97f62f42fe1beabeac2b6e8.jpg', 'Frango grelhado', 4, 'Frango grelhado acompanha arroz integral, mix legumes cozidos e salada.', '20.00'),
(6, '839f6b43fa0f237103ecf291634fac8f.jpg', 'Frango empanado', 2, 'Delicioso frango empanado, acompanha arroz, feijão e salada e fritas.', '18.00'),
(7, 'f207f319651bbaae7db6d179f8d1a7f2.jpg', 'Omelete de queijo', 2, 'Omelete recheado com queijo, acompanha arroz, feijão, salada e fritas', '18.00'),
(8, 'd0f688d313f9e0268be0023968ae111d.jpg', 'Virado à Paulista', 1, 'Acompanha arroz, tutu de feijão, couve, ovo, bisteca, calabresa, torresmo e banana frita. \r\nDisponível todas as segundas.', '30.00'),
(9, 'be83239bd431cfe47ecc78ce4d0afbd2.jpg', 'Contra-filé', 1, 'Delicioso contra-filé. Disponível todas as terças.', '20.00'),
(10, 'a9dbcc9f825f7b0a1876aa5078043903.jpg', 'Feijoada', 1, 'Arroz, feijoada (carne seca, bacon, linguiça e costelinha), couve, torresmo ou calabresa e farofa.\r\nDisponível todas as quartas.', '30.00'),
(11, '62fd254e7b6f7b8817ce4e4ba2d7f74c.jpg', 'Macarronada com frango assado', 1, 'Deliciosa macarronada, acompanha frango assado. Disponível todas as quintas.', '30.00'),
(12, 'b43982d54cfee202d9cacb02ae9b7af2.jpg', 'Filé de frango com molho de camarão', 1, 'Delicioso filé de frango, acompanha arroz, molho de camarão e purê de batatas. Disponível todas as sextas.', '30.00'),
(13, '457b1583a07a08e098d59effd8f63866.jpg', 'Suco de laranja', 3, 'Suco de laranja gelado.', '7.00'),
(14, 'f07eb9af259e03e64151297a53eaee6f.jpg', 'Coca-cola', 3, 'Coca-cola gelada', '5.00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `nome_categoria` varchar(100) NOT NULL,
  `data_categoria` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categoria`
--

INSERT INTO `categoria` (`id`, `nome_categoria`, `data_categoria`) VALUES
(1, 'Prato do Dia', '2019-11-21 05:04:17'),
(2, 'Prato Feito', '2019-11-21 05:04:43'),
(3, 'Bebidas', '2019-11-21 05:04:48'),
(4, 'Fitness', '2019-11-21 05:45:40');

-- --------------------------------------------------------

--
-- Estrutura da tabela `comanda`
--

CREATE TABLE `comanda` (
  `id` int(11) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nome` varchar(200) DEFAULT NULL,
  `mesa` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `comanda`
--

INSERT INTO `comanda` (`id`, `data`, `nome`, `mesa`, `status`) VALUES
(2, '2019-12-01 15:26:56', 'Amanda Cristina', 6, 1),
(4, '2019-12-02 16:43:58', 'Daniela', 5, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `assunto` varchar(50) NOT NULL,
  `mensagem` varchar(200) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `nome`, `email`, `assunto`, `mensagem`, `data`) VALUES
(1, 'Débora', 'deehmiyake@gmail.com', 'duvida', 'Qual é o horário de funcionamento?', '2019-10-27 22:42:40'),
(2, 'Daniela', 'danielamirandaa9@gmail.com', 'sugestao', 'Vocês já pensaram em trabalhar com tortas?', '2019-10-27 22:42:53'),
(3, 'Luana', 'luanaguedes@gmail.com', 'problema', 'Eu esqueci minha bolsa no restaurante.', '2019-10-27 22:43:02'),
(4, 'Ana', 'anaana@gmail.com', 'duvida', 'Vocês trabalham em feriados?', '2019-10-27 22:43:30'),
(5, 'Daniela Miranda de Almeida', 'danielamirandaa9@gmail.com', 'dúvida', 'Tenho um problema!!', '2019-11-28 02:01:19');

-- --------------------------------------------------------

--
-- Estrutura da tabela `itens_do_pedido`
--

CREATE TABLE `itens_do_pedido` (
  `id_item` int(11) NOT NULL,
  `nome_produto` varchar(200) NOT NULL,
  `subtotal` varchar(200) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `preco` varchar(100) NOT NULL,
  `cardapio_id` int(11) DEFAULT NULL,
  `comanda_id` int(11) NOT NULL,
  `data_venda` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `itens_do_pedido`
--

INSERT INTO `itens_do_pedido` (`id_item`, `nome_produto`, `subtotal`, `quantidade`, `preco`, `cardapio_id`, `comanda_id`, `data_venda`) VALUES
(6, 'Calabresa', '36', 2, '18.00', 2, 2, '2019-12-01 23:01:34'),
(7, 'Calabresa', '54', 3, '18.00', 2, 2, '2019-11-01 23:01:42'),
(8, 'Suco de laranja', '28', 4, '7.00', 13, 2, '2019-12-01 23:01:50'),
(15, 'Calabresa', '54', 3, '18.00', 2, 4, '2019-12-02 17:28:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cardapio`
--
ALTER TABLE `cardapio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comanda`
--
ALTER TABLE `comanda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `itens_do_pedido`
--
ALTER TABLE `itens_do_pedido`
  ADD PRIMARY KEY (`id_item`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cardapio`
--
ALTER TABLE `cardapio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `comanda`
--
ALTER TABLE `comanda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `itens_do_pedido`
--
ALTER TABLE `itens_do_pedido`
  MODIFY `id_item` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
